<?php

session_start();

//Set manually for demonstration purposes only;
$_SESSION['idusuario'] = 1;

require_once "inc/constants.php";
require_once "vendor/autoload.php";
require_once ABSPATH.'/inc/autoload.php';
require_once ABSPATH.'/inc/functions.php';

//LOG
$Log = new Log();
$Browser = new Browser();

$Log
    ->setIdusuario( $_SESSION['idusuario'] )
    ->setConteudo('Conteúdo teste')
    ->setDataTransacao(CURR_DATETIME)
    ->setOperacao('INSERT')
    ->setBrowser( $Browser->getBrowser() )
;

$LogModel = new LogModel($Log);
$LogModel->insert();

//USUARIO
$Usuario = new Usuario();

$Usuario
    ->setNome('José')
    ->setIdade(49)
;

$UsuarioModel = new UsuarioModel( $Usuario );
$UsuarioModel->insert();

//Prompting the inserted data...
$rsLog = $LogModel->select(null, TRUE);
$rsUsuario = $UsuarioModel->select(null, TRUE);

echo "<pre>";

$pr1 = print_r( $rsLog, 1 );
$pr2 = print_r( $rsUsuario, 1 );

echo "<hr><h3>Inserções <i>Log</i></h3>";
echo $pr1;

echo "<hr><h3>Inserções <i>Usuario</i></h3>";
echo $pr2;

echo "</pre>";

#For manual PDO actions
//$LogModel->getConn()->getPdo();

#Public methods
//$LogModel->getConn()->getLastExecutedQuery();
//$LogModel->getConn()->getLastInsertedId();
//$LogModel->getConn()->getLastPreparedStmtQuery();
//$LogModel->getConn()->getLastTransactionType();







