<?php

require_once ABSPATH."/inc/constants.php";

function morre($v = 'Morreu'){
    if (is_array($v)) {
        echo "<pre>";
        print_r($v);
    } else {
        var_dump($v);
    }

    die();
}

/**
 * @param $var array|string|integer
 * @return bool
 */
function _empty( $var, $allowZeroAndFalse = TRUE ){
    //Check if it's array
    if( is_array( $var ) )
        return ( count( $var ) === 0 );

    if( !$allowZeroAndFalse ){
        return ( ( !!$var ) === FALSE );
    }else{
        return is_null( $var );
    }

}

function get_next_key_array($array,$key){
    $keys       = array_keys($array);
    $nextKey    = NULL;
    $position   = array_search($key, $keys);

    if (isset($keys[$position + 1]))
        $nextKey = $keys[$position + 1];

    return $nextKey;
}

/**
 * Iterates template paths to add to Twig Loader
 * @throws Exception
 * @return void
 * */
function includeTwigPaths( array $paths, Twig\Loader\FilesystemLoader &$loader ) : void{
    if( count( $paths ) > 0 )
        foreach( $paths as $path ) $loader->addPath( $path );
    else
        throw new Exception( '\$loader must not be empty' );
}

function getAppConstants(){
    return [
        'SITE'             => SITE
        ,'APP_NAME'         => APP_NAME
        ,'HOME_URI'         => HOME_URI
        ,'ABS_HOME_URI'     => ABS_HOME_URI
        ,'ABSPATH'          => ABSPATH
        ,'CURR_DATE'        => CURR_DATE
        ,'CURR_DATETIME'    => CURR_DATETIME
    ];
}

/**
 * It's a strip_tags enhanced. Adds a white space between words from different tags
 * @param string $string
 * @return string
 * */
function rip_tags($string){
    // ----- remove HTML TAGs -----
    $string = preg_replace ('/<[^>]*>/', ' ', $string);

    // ----- remove control characters -----
    $string = str_replace("\r", '', $string);    // --- replace with empty space
    $string = str_replace("\n", ' ', $string);   // --- replace with space
    $string = str_replace("\t", ' ', $string);   // --- replace with space

    // ----- remove multiple spaces -----
    $string = trim(preg_replace('/ {2,}/', ' ', $string));

    return $string;
}
