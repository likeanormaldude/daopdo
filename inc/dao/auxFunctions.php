<?php

/*
 * Returns the whole insert querystring, starting after * the 'table'
 *
 * $matrixVal = array(
 *       array('idUsuario', 1, PDO::PARAM_INT)
 *      ,array('senha', 'semsenha', PDO::PARAM_STR)
 * );
 *
 * //$queryInsert = "INSERT INTO usuario (idUsuario, senha) VALUES(:idUsuario, :senha) "
 *
 * */

/**
 *
 * Returns the whole insert querystring, starting after 'table' keyword
 * @return string The missing piece of insert. i.e <i>"(user, pass) VALUES
 * (:user, :pass)"</i>
 * @uses $matrixVal = array(<br>
 *       &nbsp;&nbsp;&nbsp;array('idUsuario', 1, PDO::PARAM_INT)<br>
 *      &nbsp;&nbsp;,array('senha', 'semsenha', PDO::PARAM_STR)<br>
 * );
 */
function getInsertString(array $matrixVal){
    $arrFields = [];
    $arrValues = [];
    $values = " VALUES ";

    foreach ($matrixVal as $v) {
        $arrValues[] = ':' . $v[0];
        $arrFields[] = $v[0];
    }

    $fields = ' ( ' . implode(", ", $arrFields) . ' )';
    $values .= ' ( ' . implode(", ", $arrValues) . ' )';

    return $fields . $values;
}

/**
 * @param array|null|boolean $matrixVal
 * @param array $exceptions
 * @return string
 */

/**
 * Returns the correct string to the 'SET' clause in update queries.
 * (:user, :pass)"
 * @param array|null|boolean $matrixVal
 * @param array $exceptions
 * @return string The missing piece of the querystring. i.e
 * <i>"usuario = :usuario, senha = :senha"</i>
 */
function getUpdateFieldsString(array $matrixVal, $exceptions ){
    $arrFields = [];

    foreach ( $matrixVal as $v ) {
        if (in_array( $v[0], $exceptions )) continue;

        $arrFields[] = $v[0].' = :'.$v[0]; // "usuario = :usuario"
    }

    return implode(',', $arrFields);
}

/**
 * Returns the correct string to the 'WHERE' clause in <i>SELECT</i> queries <p>
 *
 * <i>Ex.:</i><code>" usuario = :usuario, AND senha = :senha"</code>
 * @param array|null|boolean $matrixVal
 * @param array $exceptions
 * @return string The missing piece of the querystring. i.e
 * <i>"usuario = :usuario AND senha = :senha"</i>
 */
function getWhereFieldsString(array $matrixVal){
    $arrFields = [];

    foreach ( $matrixVal as $v ) {
        $arrFields[] = $v[0].' = :'.$v[0]; // "usuario = :usuario"
    }

    if( count( $arrFields ) > 0 ){
        return ' AND '.implode(' AND ', $arrFields);
    }else{
        return '';
    }
}



/**
 * @param mixed $v
 * @return bool|int The PDO constant corresponding the type – <i>PDO::PARAM_STR or PDO::PARAM_INT</i>
 */
function getPdoVarType($v){
    switch (gettype($v)) {
        case "string":
            return PDO::PARAM_STR;
        case "double":
            return PDO::PARAM_STR;
        case "integer":
            return PDO::PARAM_INT;
        default:
            return FALSE;
    }
}

/**
 * @return string The type of transaction executed on DB – <i>(SELECT, UPDATE, INSERT or DELETE)</i>
 * */
function getDbTransactionType($queryString){
    if (!$queryString) return FALSE;

    $cleanQueryString = trim(preg_replace('/\s{2}/', '', $queryString));
    $parts = explode(" ", $cleanQueryString);
    $transactionType = $parts[0]; //In a clean queryString, the transaction type will always be the first space

    return strtoupper($transactionType);
}

