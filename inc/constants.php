<?php

//__________________________GENERAL APP SETTINGS__________________________\\
/**
 * IMPORTANT!
 * Development Environment. Enables debug output on screen, for example.
 * */
define( "DEV_ENV", TRUE );

define( "APP_NAME", "daopdo");
define( "HOME_URI", "http://".$_SERVER['HTTP_HOST']);
define( "ABS_HOME_URI", HOME_URI.'/'.APP_NAME);
define( 'ABSPATH', str_replace( '\\', '/', dirname(dirname(__FILE__)) ) );
define( "DEBUG_FILE_PATH", ABSPATH.'/tmp/debugHTML.html' );

//__________________________GENERAL APP SETTINGS__________________________//

//__________________________DATE AND TIME SETTINGS________________________\\
date_default_timezone_set('America/Sao_Paulo');
define("CURR_DATE", date('Y-m-d'));
define("CURR_DATETIME", date('Y-m-d H:i:s'));
//__________________________DATE AND TIME SETTINGS________________________//


