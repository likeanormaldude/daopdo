<?php

require_once ABSPATH . "/inc/classes/Browser.php";
require_once ABSPATH . "/inc/constants.php";
require_once ABSPATH . "/inc/dao/constants.php";
require_once ABSPATH . "/inc/dao/auxFunctions.php";


class DB implements iSingleTon{

    const SELECT_TRANSC_TYPE = 'SELECT';
    const UPDATE_TRANSC_TYPE = 'UPDATE';
    const INSERT_TRANSC_TYPE = 'INSERT';
    const DELETE_TRANSC_TYPE = 'DELETE';

    private
        $host
    ,$db_name
    ,$debug
    ,$password
    ,$user
    ,$charset
    ,$lastFetchedData           = []
    ,$lastPreparedStmtQuery     = NULL
    ,$stmt                      = NULL
    ,$pdo			            = NULL
    ,$lastInsertedId		    = NULL
    ,$lastExecutedQuery         = NULL
    ,$lastTransactionType       = NULL
    ,$lastCaughtException       = NULL
    ;

    /**
     * This attribute is used to save the current state of this class. <p> Whenever
     * <i>Log</i> is <b>TRUE</b> The last transaction will always be <i>INSERT</i>,
     * for example</p>
     * @var array $dbvars
     * */
    private $dbvars;

    /**
     * @var DB $SingleTon
     * */
    private static $SingleTon;

    /**
     * @var DebugPDOStatement $savedStmt The saved PDO stmt variable.
     * */
    private $savedStmt;

    /**
     * @return null
     */
    public function getLastTransactionType(){
        return $this->lastTransactionType;
    }

    /**
     * @return string
     */
    public function getLastPreparedStmtQuery(){
        return $this->lastPreparedStmtQuery;
    }

    public function __construct(
        $host      = NULL
        ,$db_name   = NULL
        ,$password  = NULL
        ,$user      = NULL
        ,$charset   = NULL
        ,$debug     = NULL
    ){
        $this->host     = defined( 'HOSTNAME'    	) 	? HOSTNAME    	: $host;
        $this->db_name  = defined( 'DB_NAME'     	) 	? DB_NAME     	: $db_name;
        $this->password = defined( 'DB_PASSWORD' 	) 	? DB_PASSWORD 	: $password;
        $this->user     = defined( 'DB_USER'     	) 	? DB_USER     	: $user;
        $this->charset  = defined( 'DB_CHARSET'  	) 	? DB_CHARSET  	: $charset;
        $this->debug    = defined( 'DEBUG'		    ) 	? DEBUG 		: $debug;

        $this->connect();
    }

    private function connect(){
        // In future releases, this will be prepared to be switched to other db types.
        $dsn  = "mysql:host=".$this->host.";";
        $dsn .= "dbname=".$this->db_name.";";
        $dsn .= "charset=".$this->charset.";";

        $options = [
            PDO::ATTR_CASE => PDO::CASE_NATURAL
        ];

        try{
            $this->setPdo( new DebugPDO( $dsn, $this->user, $this->password, $options ) );

            if ($this->debug === true) // Driver option
                $this->getPdo()->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

            unset(
                $this->host
                ,$this->db_name
                ,$this->password
                ,$this->user
                ,$this->charset
            );

            /* DEBUG only */
            //echo "Conectado com sucesso!";
        } catch (PDOException){
            $this->_invokeDebugHTML( DB_ERR_CONN );
        }
    }

    /**
     * @return array
     */
    public function getLastFetchedData(): array{
        return $this->lastFetchedData;
    }



    /**
     * Suppresses verbose <i>DebugHTML</i> call in this class and also assigns the last
     * error message to the attribute 'lastCaughtException'
     * */
    private function _invokeDebugHTML( $content ){
        $this->lastCaughtException = $content;
        DebugHTML::getInstance( $content )->debug(DebugHTML::DB_DEBUG_CONTENT, $this);
    }

    /**
     * Save the DB State regarding to last_* variables
     * */
    private function _initDBState(){
        $this->savedStmt = $this->getStmt();
        $this->dbvars = [
            'lastPreparedStmtQuery'    => $this->lastPreparedStmtQuery
            ,'lastInsertedId'           => $this->lastInsertedId
            ,'lastExecutedQuery'        => $this->lastExecutedQuery
            ,'lastTransactionType'      => $this->lastTransactionType
        ];
    }

    /**
     * Reassign all last_* variables of DB class
     * @return void
     * */
    private function _getSavedState(){
        foreach( $this->dbvars as $dbvarname => $value ){
            $this->{$dbvarname} = $value;
        }

        // Reset dbvars
        $this->dbvars = [];

        // Reassigns the stmt variable based on the lastExecuted query.
        // lastpreparedStmt will be removed in future release of this repo
        $this->setStmt( $this->getPdo()->query( $this->lastExecutedQuery ) );
    }

    /**
     * This methods checks weather if is a <i>SELECT</i>. If it is, it <p>
     * Should return the records, if possible.</p><p> In other transactions
     * like <i>DELETE</i>, it's important to check whether if it was succeeded or not.</p>
     * @param $log
     * @return $this|array|bool
     */
    private function _result($log ){
        $rowCount = $this->getStmt()->rowCount();

        // When $log is TRUE, the last transaction type will always be 'insert', breaking the logic of the app context.
        // What matters here is the transaction of the last query regardless the log process

        $this->_initDBState();

        if( $log || ( DEBUG_ALL_QUERIES === TRUE ) )
            $this->log();

        // Restore the previous state before the log process
        $this->_getSavedState();

        if( $rowCount ){
            //When the statement is SELECT, then should return its array
            if( $this->lastTransactionType === self::SELECT_TRANSC_TYPE ){
                $this->lastFetchedData = $this->getStmt()->fetchAll( PDO::FETCH_ASSOC );
                return $this->lastFetchedData;
            }

            return $this;
        }

        return FALSE;
    }

    /**
     * @return bool
     * @throws Exception
     * */
    private function log(){

        $Browser    = new Browser();
        $Log        = new Log();

        $Log
            ->setIdusuario( $_SESSION['idusuario'] )
            ->setOperacao( $this->lastTransactionType )
            ->setConteudo( $this->lastExecutedQuery )
            ->setBrowser( $Browser->getBrowser() )
        ;

        $LogModel = new LogModel( $Log );

        try{
            if( $LogModel->insert() ) return TRUE;
            else throw new Exception( LOG_ERROR );
        }catch(Exception $e){
            $this->_invokeDebugHTML( $e->getMessage() );
        }

        return FALSE;
    }

    /**
     * @param string $host
     * @param string $db_name
     * @param string $password
     * @param string $user
     * @param string $charset
     * @param bool $debug
     * @return DB
     */
    public static function getInstance(
        $host      = NULL
        ,$db_name   = NULL
        ,$password  = NULL
        ,$user      = NULL
        ,$charset   = NULL
        ,$debug     = NULL
    ){

        if(!self::$SingleTon) {  //Guarantee just one instance

            $host     = defined( 'HOSTNAME'    	) 	? HOSTNAME    	: $host;
            $db_name  = defined( 'DB_NAME'     	) 	? DB_NAME     	: $db_name;
            $password = defined( 'DB_PASSWORD' 	) 	? DB_PASSWORD 	: $password;
            $user     = defined( 'DB_USER'     	) 	? DB_USER     	: $user;
            $charset  = defined( 'DB_CHARSET'  	) 	? DB_CHARSET  	: $charset;
            $debug    = defined( 'DEBUG'		    ) 	? DEBUG 		: $debug;

            self::$SingleTon = new DB($host, $db_name, $password, $user, $charset, $debug);

        }

        return self::$SingleTon;
    }

    //CRUD Methods

    /**
     * @param string $table
     * @param array $matrixVal
     * @param bool $log
     * @return bool|$this
     * */
    public function insert( $table, array $matrixVal, $log = FALSE ){
        if( !$this->getPdo() || _empty( $matrixVal ) ) return FALSE;

        try{

            $preparedStmt   = "INSERT INTO ".$table.getInsertString( $matrixVal );
            $this->setStmt( $this->getPdo()->prepare( $preparedStmt ) );

            foreach( $matrixVal as $v ){ $this->getStmt()->bindParam( $v[0], $v[1], $v[2] ); }

            $this->lastPreparedStmtQuery    = $preparedStmt;
            $this->lastTransactionType      = self::INSERT_TRANSC_TYPE;

            $this->getStmt()->execute();
            $this->lastExecutedQuery    = $this->getStmt()->debugQuery();

            //Check logging
            if( $log || DEBUG_ALL_QUERIES === TRUE || DEBUG_ALL_INSERT_TRANSACTIONS === TRUE ) $this->log();

            if( $this->lastInsertedId = (int) $this->getPdo()->lastInsertId() ){
                if( $this->getStmt()->rowCount() ) return $this;
                else throw new Exception(DB_ERR_INSERT);
            }else{
                throw new Exception(DB_ERR_INSERT);
            }

        }catch( Exception $e ){
            $this->_invokeDebugHTML( $e->getMessage() );
        }

        return FALSE;
    }

    public function update($table, array $matrixVal, $where, $exceptions = [], $log = FALSE){
        if( !$this->getPdo() || _empty( $matrixVal ) ) return FALSE;

        try{
            $where = ' WHERE '.preg_replace( '/(where)/i', '', $where );

            $preparedStmt   = "UPDATE ".$table." SET ".getUpdateFieldsString( $matrixVal, $exceptions ).' '.$where;
            $this->setStmt( $this->getPdo()->prepare( $preparedStmt ) );

            foreach( $matrixVal as $v ) $this->getStmt()->bindParam($v[0], $v[1], $v[2]);

            $this->lastPreparedStmtQuery    = $preparedStmt;
            $this->lastTransactionType      = self::UPDATE_TRANSC_TYPE;

            $this->getStmt()->execute();

            $this->lastExecutedQuery = $this->getStmt()->debugQuery();

            //Check logging
            if( $log || DEBUG_ALL_QUERIES === TRUE || DEBUG_ALL_UPDATE_TRANSACTIONS === TRUE ) $this->log();

            return TRUE;
        }catch( Exception $e ){
            $this->_invokeDebugHTML( $e->getMessage() );
        }

        return FALSE;
    }

    public function delete($table, $where, array $matrixVal = [], $log = FALSE){
        if( !$this->getPdo() ) return FALSE;

        try{

            $where = ' WHERE '.preg_replace( '/(where)/i', '', $where );
            $preparedStmt   = "DELETE FROM ".$table.' '.$where;
            $this->setStmt( $this->getPdo()->prepare( $preparedStmt ) );

            if( !_empty( $matrixVal ) )
                foreach( $matrixVal as $v ) $this->getStmt()->bindParam($v[0], $v[1], $v[2]);

            $this->lastPreparedStmtQuery    = $preparedStmt;
            $this->lastTransactionType      = self::DELETE_TRANSC_TYPE;

            $this->getStmt()->execute();
            $this->lastExecutedQuery    = $this->getStmt()->debugQuery();

            //Check logging
            if( $log || DEBUG_ALL_QUERIES === TRUE || DEBUG_ALL_DELETE_TRANSACTIONS === TRUE ) $this->log();

            if( $this->getStmt()->rowCount() ){
                return $this;
            }else{
                throw new Exception( DB_ERR_DELETE );
            }
        }catch( Exception $e ){
            $this->_invokeDebugHTML( $e->getMessage() );
        }

        return FALSE;
    }

    /**
     * @param string $query
     * @param array $matrixVal
     * @param bool $log
     *
     * @return array|bool|DB
     */
    public function query( $query, array $matrixVal = [], $log = FALSE ){
        if( !$this->getPdo() ) return FALSE;

        try {

            if( count( $matrixVal ) > 0 ){

                $this->lastPreparedStmtQuery = $query;
                $this->lastTransactionType  = getDbTransactionType( $query );
                $this->setStmt( $this->getPdo()->prepare( $query ) );

                //Binds the params
                foreach ($matrixVal as $v) $this->getStmt()->bindParam($v[0], $v[1], $v[2]);

                $this->getStmt()->execute();
                $this->lastExecutedQuery    = $this->getStmt()->debugQuery();
            }else{
                $this->lastExecutedQuery    = $query;
                $this->lastTransactionType  = getDbTransactionType( $this->lastExecutedQuery );
                $this->setStmt( $this->getPdo()->query( $query ) );
            }

            if( $this->getStmt()->rowCount() ) {
                // Check query ok
                return $this->_result($log);
                // Rows not affected. Check if it is SELECT and return its result
            }else if( $this->lastTransactionType === self::SELECT_TRANSC_TYPE ){
                return $this->getStmt()->fetchAll( PDO::FETCH_ASSOC );
            }else{
                throw new Exception(UKN_ERR_QUERY);
            }

        }catch( Exception $e ){
            $this->_invokeDebugHTML( $e->getMessage() );
        }

        return FALSE;
    }

    //CRUD Methods

    //Getter and setters

    /**
     * @return PDO
     */
    public function getPdo(){
        return $this->pdo;
    }

    /**
     * @param PDO $pdo
     * @return DB
     */
    private function setPdo($pdo){
        $this->pdo = $pdo;
        return $this;
    }

    /**
     * @return DebugPDOStatement
     */
    private function getStmt(){
        return $this->stmt;
    }

    /**
     * @param PDOStatement $stmt
     * @return DB
     */
    private function setStmt( PDOStatement $stmt){
        $this->stmt = $stmt;
        return $this;
    }

    /**
     * @return integer
     */
    public function getLastInsertedId(){
        return $this->lastInsertedId;
    }

    /**
     * @return string
     */
    public function getLastExecutedQuery(){
        return $this->lastExecutedQuery;
    }

    //Getter and setters

}//class DB
