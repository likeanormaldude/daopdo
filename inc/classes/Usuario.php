<?php

class Usuario extends MainClass implements iSingleTon {

    /**
     * @var Usuario $SingleTon
     * */
    private static $SingleTon;

    /**
     * @var int $idusuario
     * */
    private $idusuario;

    /**
     * @var string $nome
     * */
    private $nome;

    /**
     * @var int $idade
     * */
    private $idade;

    public function __construct(){
        $this->table = 'usuario';
    }

    /**
     * @return int
     * @return self
     */
    public function getIdusuario(): int{
        return $this->idusuario;
    }

    /**
     * @param int $idusuario
     */
    public function setIdusuario(int $idusuario): self{
        $this->idusuario = $idusuario;
        $this->pushMatrixVal($idusuario);
        return $this;
    }

    /**
     * @return string
     * @return self
     */
    public function getNome(): string{
        return $this->nome;
    }

    /**
     * @param string $nome
     */
    public function setNome(string $nome): self{
        $this->nome = $nome;
        $this->pushMatrixVal($nome);
        return $this;

    }

    /**
     * @return int
     * @return self
     */
    public function getIdade(): int{
        return $this->idade;
    }

    /**
     * @param int $idade
     */
    public function setIdade(int $idade): self{
        $this->idade = $idade;
        $this->pushMatrixVal($idade);
        return $this;
    }

    /**
     * @return self
     * */
    public static function getInstance(){
        //Guarantee just one instance
        if(!self::$SingleTon) self::$SingleTon = new Usuario();

        return self::$SingleTon;
    }


}