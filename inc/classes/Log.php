<?php

class Log extends MainClass implements iSingleTon{

    private static $SingleTon;

    private
         $idusuario
        ,$operacao
        ,$conteudo
        ,$dataTransacao
        ,$browser
    ;

    function __construct(){
        $this->table = 'log';
    }

    public static function getInstance(){
        //Guarantee just one instance
        if(!self::$SingleTon) self::$SingleTon = new Log();

        return self::$SingleTon;
    }

    /**
     * @return mixed
     */
    public function getIdusuario(){ return $this->idusuario; }

    /**
     * @param int $idusuario
     * @return self
     */
    public function setIdusuario($idusuario){
        $this->idusuario = $idusuario;
        $this->pushMatrixVal($idusuario);
        return $this;
    }

    /**
     * @return mixed
     */
    public function getOperacao(){ return $this->operacao; }

    /**
     * @param mixed $operacao
     * @return self
     */
    public function setOperacao($operacao){
        $this->operacao = $operacao;
        $this->pushMatrixVal($operacao);
        return $this;
    }

    /**
     * @return mixed
     */
    public function getConteudo(){ return $this->conteudo; }

    /**
     * @param string $conteudo
     * @return self
     */
    public function setConteudo($conteudo){
        $this->conteudo = $conteudo;
        $this->pushMatrixVal($conteudo);
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDataTransacao(){ return $this->dataTransacao; }

    /**
     * @param string $dataTransacao
     * @return self
     */
    public function setDataTransacao($dataTransacao){
        $this->dataTransacao = $dataTransacao;
        $this->pushMatrixVal($dataTransacao);
        return $this;
    }

    /**
     * @return mixed
     */
    public function getBrowser(){ return $this->browser; }

    /**
     * @param string $browser
     * @return self
     */
    public function setBrowser( string $browser ){
        $this->browser = $browser;
        $this->pushMatrixVal($browser);
        return $this;
    }

}