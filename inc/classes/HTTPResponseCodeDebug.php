<?php

/**
 * Whenever a default template should be printed out on screen, this class must
 * come in play. <p>Errors like 404 (not found) or 500 (server internal error)
 * has a default template</p>
 * */
class HTTPResponseCodeDebug{

    /**
     * @var HTTPResponseCodeDebug $SingleTon
     * */
    private static $SingleTon;

    /**
     * @var int $htmlCode The HTTP response code
     * @see https://developer.mozilla.org/en-US/docs/Web/HTTP/Status
     * */
    private $htmlCode;

    /**
     * @var array $mTemplate Template matrix, binding its html reponse code to the template name
     * */
    private $mTemplate;

    /**
     * @var Twig\Loader\FilesystemLoader $loader
     * */
    private $loader;

    /**
     * @var string $twigTemplate
     * */
    private $twigTemplate;

    /**
     * @var array $contextParams
     * */
    private $contextParams;

    /**
     * @param int $htmlCode <p>The html response code to debug. Can be one of the following:
     * <table>
     * <tr valign="top">
     * <td>htmlCode</td>
     * <td>Description</td>
     * </tr>
     * <tr>
     * <td>404</td>
     * <td>Not Found</td>
     * </tr>
     * <tr>
     * <td>500</td>
     * <td>Server Internal Error</td>
     * </tr>
     * </table>
     * </p>
     * */
    public function __construct( $htmlCode ){
        $this->htmlCode = $htmlCode;

        $this->mTemplate = [
            '404' => '404.html.twig',
            '500' => '500.html.twig'
        ];

        $this->twigTemplate = $this->mTemplate[$htmlCode];

        //Initializing objects
        $this->loader = new Twig\Loader\FilesystemLoader();

        includeTwigPaths([
            ABSPATH . '/templates'
            , ABSPATH . '/templates/sample-and-debug/HttpResponseCodesDebug/'
        ], $this->loader);

        $this->_initDefaultContextParams();
    }


    private function _initDefaultContextParams(){
        $this->contextParams = [
             'ABSPATH' => ABSPATH
            ,'ABS_HOME_URI' => ABS_HOME_URI
            ,'pathFromRoot' => 'templates/sample-and-debug/HttpResponseCodesDebug'
        ];
    }

    public static function getInstance( int $htmlCode ){
        //Guarantee just one instance
        if(!self::$SingleTon)
            self::$SingleTon = new HTTPResponseCodeDebug( $htmlCode );

        return self::$SingleTon;
    }

    public function render( array $contextParams = [] ){
        $cp = array_merge( $this->contextParams, $contextParams );

        try {
            $twig = new Twig\Environment( $this->loader );
            $template = $twig->load( $this->twigTemplate );

            echo $template->render( $cp );
        } catch (Exception $e) {
            die ('ERROR: ' . $e->getMessage());
        }

    }
}