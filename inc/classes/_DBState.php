<?php

require_once ABSPATH . "/inc/constants.php";


/**
 * Do not instantiate this class in the app. For DB due purposes only
 * */
class _DBState implements iSingleTonModel {

    /**
     * @var _DBState $SingleTon
     * */
    private static $SingleTon;

    /**
     * @var array $dbvars
     * */
    private $dbvars;

    /**
     * @var DB $DB DB active connection instance
     * */
    private $DB;

    public function __construct( DB &$DB ){
        $this->DB = $DB;
    }

    public function initDBState(){
        $this->dbvars = [
             'lastPreparedStmtQuery'    => $this->DB->getLastPreparedStmtQuery()
            ,'lastInsertedId'           => $this->DB->getLastInsertedId()
            ,'lastExecutedQuery'        => $this->DB->getLastExecutedQuery()
            ,'lastTransactionType'      => $this->DB->getLastTransactionType()
        ];
    }

    public function getSavedState(){
        foreach( $this->dbvars as $dbvarname => $value ){
//            $this->DB
        }
    }

    /**
     * @param DB $DB
     * @return self
     */
    public static function getInstance( $DB ){
        if(!self::$SingleTon)//Guarantee just one instance
            self::$SingleTon = new _DBState( $DB );

        return self::$SingleTon;
    }

}