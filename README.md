# DAO PHP

A project structure pattern designed to work with PDO DAO based class with a bunch of cool features! 

## Installation
```sh
git clone https://gitlab.com/likeanormaldude/daophp.git
cd daophp
git checkout dev
composer install
# In your browser: http://{your_server}/{you_alias}/example.php
```

## Project Standards
DAO follows the idea of an object passed to the constructor of a model (i.e ```new LogModel($Log))``` .

All classes must start with:
```php
class YourObjectClassName extends MainClass implements iSingleTon ...
```

In ```set``` methods on object classes, right before the end add this:
```php
$this->pushMatrixVal($idusuario);
```

### Sample Database _temp_
Run the following script in the DBMS:
```./_sql/dump-lottus2202211056.sql```

## Quickstart
Use the [./example.php] as a quickstart file.

#### DebugHTML
Note: In future releases this feature will only be enable according to the debug constants on ```./inc/constants.php``` 

Another interesting resource of this repo is the DebugHTML. In some environments like external web app hosting, you cannot debug properly your app. Getting a physical debug html file can help you with it.
By default, any catch exception caught in DB class will be prompted on screen, with the help of [Twig].

#### Contribuition
This is a collaborative repository. Checkout to ```dev``` branch to make your pull request on any sight of possible improvement or correction. Thank you!

[//]: # 
   [Composer Website]: <https://getcomposer.org/>
   [Twig]: <https://twig.symfony.com/>
   [./example.php]: <https://gitlab.com/likeanormaldude/daopdo/-/blob/dev/example.php>
   



