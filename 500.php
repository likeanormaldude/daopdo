<?php

require_once "inc/constants.php";
require_once ABSPATH . '/inc/autoload.php';
require_once ABSPATH . '/vendor/autoload.php';
require_once ABSPATH . '/inc/functions.php';

try {
    HTTPResponseCodeDebug::getInstance(500)->render();
} catch (Exception $e) {
    die ('ERROR: ' . $e->getMessage());
}



